﻿using BusinessLogic;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace CMS_API.Controllers
{
    [RoutePrefix("api/fixtures")]
    public class FixturesController : ApiController
    {
        FixturesBL bl = new FixturesBL();
        [Route("")]
        public JsonResult<IQueryable<Table>> GetFixture()
        {
            return Json(bl.GetFixtures());
        }

        [Route("{country}")]
        public JsonResult<IQueryable<Table>> GetFixturesByCountry(string country)
        {
            return Json(bl.GetFixturesByCountry(country));
        }
    }
}
